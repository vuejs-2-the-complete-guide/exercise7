import Vue from 'vue'
import App from './App.vue'

export const EventBus = new Vue({
  methods: {
    change(data) {
      this.$emit('changeServer', data);
    },
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
